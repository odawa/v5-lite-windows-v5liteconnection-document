# 概要
## このドキュメントについて
本ドキュメントは、V-CUBE ミーティング 5 Windows版を外部から利用するためのSDKの情報です。

## 構成要素
### 会議アプリ
会議を行うアプリです。後述のAPIを使って制御することが可能です。

#### 会議アプリの起動
以下のコマンドで会議アプリを起動できます。

```
V-CUBE_Meeting_5_Lite_Conference.exe rootname endpoint
```

- rootname
    - プロセス間通信に使うアドレスのルート文字列です。
    - SDKを利用した他の製品と衝突しないような値にする必要があります。
    - この値が同じものは同じ製品とみなされ、会議アプリが二重起動しないようになります。
- endpoint
    - 会議アプリとのプロセス間通信の接続を一意にするためのGUID文字列です。
    - 会議アプリの起動ごとに異なるGUIDにする必要があります。

### API
会議アプリを制御するためのAPIが用意されています。
このAPIを使って、各種メッセージを会議アプリとやり取りすることで制御を行います。

メッセージのやり取りにはWCF(Windows Communication Foundation)を利用していますが、このAPIで隠蔽しており、通常は意識する必要はありません。

# API解説
## V5LiteConnection.ServerConnection
会議アプリとのプロセス間通信のコネクションを表すクラスです。
主にメッセージの送受信を担当します。

会議アプリ一つに対して一つのインスタンスが必要です。

### プロパティ
#### bool CanSendMessage
メッセージの送信が可能かどうかを示すプロパティです。このプロパティがtrueであればメッセージ送信が可能です。

### コンストラクタ
#### ServerConnection(string rootName, Guid endpoint)
ServerConnectionのコンストラクタです。

##### パラメータ
- string rootname
    - プロセス間通信に使うアドレスのルートとなる文字列です。
    - 会議アプリ起動時のコマンドライン引数の一番目に渡す必要があります。
    - SDKを利用した他の製品と衝突しないような値にする必要があります。
    - この値が同じものは同じ製品とみなされ、会議アプリが二重起動しないようになります。
- Guid endpoint
    - 会議アプリとの通信経路を一意にするためのGUIDです。
    - 会議アプリ起動時のコマンドライン引数の二番目に渡す必要があります。
    - 会議アプリの起動ごとに異なるGUIDにする必要があります。

### メソッド
#### void BeginOpen()
プロセス間通信のためのコネクションをオープンします。
このメソッドは非同期です。

このメソッドの呼び出しによって、以下のイベントが発生する可能性があります。
- OnOpened
- OnOpenTimeout
- OnOpenError

OnOpenedイベント発生後に、会議アプリへのメッセージ送信が可能になります。

#### void SendMessage(TimeSpan sendTimeout, MessageBody messageBody)
会議アプリに対してメッセージを送信します。
このメソッドは非同期です。

このメソッドの呼び出しによって、以下のイベントが発生する可能性があります。
- OnMessageSent
- OnSendMessageTimeout
- OnSendMessageError

##### パラメータ
- TimeSpan sendTimeout
    - メッセージ送信処理のタイムアウト時間です。
- MessageBody messageBody
    - 送信するメッセージです。

#### void Release()
プロセス間通信のためのコネクションを閉じ、API内部で利用している各種リソースを解放します。
このメソッドの呼び出し後、ServerConnectionインスタンスは利用できなくなります。

### イベント
#### event Action OnOpened
BeginOpenメソッドの処理が正常に終了し、なおかつ会議アプリからの接続が完了した場合に発生します。
会議アプリとのメッセージのやりとりは、このイベント発生後に可能になります。

#### event Action OnOpenTimeout
BeginOpenメソッドの処理がタイムアウトした場合に発生します。
タイムアウト後の、コネクション再オープンはサポートしていません。

#### event Action OnOpenError
BeginOpenメソッドの処理がエラーになった場合に発生します。
エラー発生後の、コネクション再オープンはサポートしていません。

#### event Action&lt;MessageBody&gt;  OnMessageSent
会議アプリへのメッセージ送信に成功した場合に発生します。
##### パラメータ
- MessageBody
    - 送信に成功したメッセージです。

#### event Action&lt;MessageBody&gt; OnSendMessageTimeout
会議アプリへのメッセージ送信がタイムアウトした場合に発生します。
###### パラメータ
- MessageBody
    - タイムアウトしたメッセージです。

#### event Action&lt;Exception, MessageBody&gt; OnSendMessageError
会議アプリへのメッセージ送信時にエラーが発生した場合に発生します。
###### パラメータ
- Exception
    - 発生したエラーです。
- MessageBody
    - エラーになったメッセージです。

#### event Action&lt;MessageBody&gt; OnMessageReceived
会議アプリからのメッセージを受信した場合に発生します。
##### パラメータ
- MessageBody
    - 受信したメッセージです。このオブジェクトのTypeプロパティを参照することで、メッセージの種類を判別することができます。

#### event Action&lt;Exception&gt; OnReceiveMessageError
会議アプリからのメッセージ受信処理でエラーが発生した場合に発生します。
###### パラメータ
- Exception
    - 発生したエラーです。

#### event Action OnClosed
BeginCloseメソッドの非同期処理が正常に終了した場合に発生します。

#### event Action OnCloseTimeout
BeginCloseメソッドの非同期処理がタイムアウトした場合に発生します。

#### event Action OnCloseError
BeginCloseメソッドの非同期処理がエラーになった場合に発生します。


## V5LiteConnection.Messages.MessageBody
会議アプリとのプロセス間通信でやり取りされるメッセージのクラスです。メッセージのインスタンスを作成するstaticメソッドを備えています。

### プロパティ
#### MessageTypes Type
メッセージの種類を表す、MessageTypes型のプロパティです。
会議アプリから受け取ったメッセージの種別を判断する場合は、このプロパティを参照します。

### メソッド
メッセージのインスタンスを作成する際には、以下のstaticメソッドを利用してください。
#### static MessageBody ConferenceConnectionInfoResponce(string token, string portalURI, string entryMethod = "", string defaultDisplayName = "", string password = "")
会議アプリに対して、会議へ接続するための情報を伝えるためのメッセージを作成します。
##### パラメータ

- string token
    - ポータルにログインして取得したトークンです。
- string portalURI
    - ポータルのURIです。
- string entryMethod
    - 入室の方法を示す文字列です。通常は指定する必要はありません。
- string defaultDisplayName
    - 表示名のデフォルト値です。通常は入室時に会議アプリで入力するため、指定する必要はありません。
- string password
    - 会議室に入室する際のパスワードです。通常は入室時に会議アプリで入力するため、指定する必要はありません。

#### static MessageBody ExitOrder()
会議アプリに対して、終了を指示するメッセージを作成します。

#### static MessageBody ShowConnectionStatusOrder()
会議アプリに対して、接続状態を表示するように指示するメッセージを作成します。


## V5LiteConnection.Messages.MessageTypes
メッセージの種類を定義したEnumです。MessageBodyのTypeプロパティに設定されます。

主に受信したメッセージの種類を判別する場合に利用します。

定義されている値は以下の通りです。

|値|説明|
|:-|:-|
|ConferenceConnectionInfoRequest|会議アプリから送信される、接続情報要求のメッセージです。このメッセージを受け取ったら、ConferenceConnectionInfoResponceメッセージで会議への接続情報を会議アプリへと伝える必要があります。|
|ConferenceConnectionInfoResponce|会議アプリへ接続情報を伝えるメッセージです。会議アプリからConferenceConnectionInfoRequestメッセージを受け取ったら、このメッセージで会議への接続情報を会議アプリへと伝える必要があります。|
|ExitOrder|会議アプリへ終了を指示するメッセージです。|
|ShowConnectionStatusOrder|会議アプリに対して、接続状態の表示を指示するメッセージです。|
|SpecifiedBandWidthInfo|入室した会議室に帯域制限が課せられていた場合、会議アプリから送信されるメッセージです。|

# APIを使った、会議入室までのフロー
APIを利用した、会議開始までの一般的なフローは以下のようになります。

1. V-CUBEのポータルにログインし、トークンを取得する。

1. プロセス間通信用にrootNameの文字列と、endpointのGUIDを用意する。
    ```
    string rootName = "sdktest";
    Guid endpoint = Guid.NewGuid();
    ```
1. ServerConnectionのインスタンスを作成する。
    ```
    serverConnection = new ServerConnection(rootName, endpoint);
    ```
1. ServerConnectionのイベントハンドラを登録する。
    ```
    serverConnection.OnMessageReceived += OnServerConnectionMessageReceived;
    ```

1. ServerConnection#BeginOpen()を呼び出す。
    ```
    serverConnection.BeginOpen()
    ```

1. 会議アプリを起動する。
    ```
    Process.Start(V-CUBE_Meeting_5_Lite_Conference.exeのパス, 任意のrootName, endpoint);
    ```

1. ServerConnection.OnMessageReceivedイベントのハンドラで、会議アプリからのConferenceConnectionInfoRequestメッセージを待つ。

1. ConferenceConnectionInfoRequestメッセージを受けたら、ConferenceConnectionInfoResponceメッセージを作成する。
    - ConferenceConnectionInfoResponceメッセージ作成時の引数として、ポータルから取得したトークンと、ポータルのURIが必要です。

1. ServerConnection#SendMessage()メソッドで作成したメッセージを送信する。
    ```
    private void OnServerConnectionMessageReceived(MessageBody messageBody)
    {
        switch (messageBody.Type)
        {
            case MessageTypes.ConferenceConnectionInfoRequest:
                SendMessage(MessageBody.ConferenceConnectionInfoResponce(トークン, ポータルURI);
                break;
            default:
                break;
        }
    }
    ```

その後、必要に応じてメッセージのやり取りを行って会議アプリを制御します。
